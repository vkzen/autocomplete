(function ($) {
    'use strict';

    $.fn.autocomplete = function(element) {

        var search = this,
            options = $.extend({}, $.fn.autocomplete.defaults),
            container = this.parent(),
            list = $('<ul class="autocomplete-list"></ul>'),
            filteredItems = [],
            selectItem = function() {
            },
            hoverItem = function() {
                list.find('.highlighted').removeClass('highlighted');
                $(this).addClass('highlighted');
            },
            unhoverItem = function() {
                $(this).removeClass('highlighted');
            },
            emptyList = function() {
                list.empty();
                var emptyItem = $('<li class="autocomplete-list-item-empty"></li>');
                emptyItem.append(options.emptyMessage);

                list.append(emptyItem);
                return;
            },
            populateList = function() {
                //loop through each item and render
                var resultsCharCount = Math.floor($(search).width() / 10.06);
                for (var idx = 0; idx < filteredItems.length; idx++) {

                    filteredItems[idx].indexOf($(search).val())

                    //create list item
                    var listItem = $('<li class="autocomplete-list-item" index="' + idx + '">' + filteredItems[idx] + '</li>');

                    //store item data in element data
                    listItem.data('item-data', filteredItems[idx]);

                    listItem.mousedown(selectItem);
                    listItem.mouseover(hoverItem);
                    listItem.mouseout(unhoverItem);

                    list.append(listItem);
                }
                return;
            };

        search.wrap('<div class="autocomplete"></div>');
        list.css('max-height', options.maxHeight + 'px');


        search.keyup(function(event) {
            $.ajax({
                url: '/complex',
                data: {'search': $(search).val()},
                type: 'GET',
                success: function(response) {
                    list.empty();
                    filteredItems = response;
                    if (filteredItems == 0)
                        emptyList();
                    else
                        populateList();
                },
                error: function(error) {
                    console.log(error)
                }
            });
        });

        search.focus(function(event) {
            list.show();
        })

        search.blur(function(event) {
            list.hide();

            //remove any highlighting
            list.find('.highlighted').removeClass('highlighted');
        });

        container = search.parent();
        container.append(list);

        /*alert($(this).width());
        alert($(this).val().length);*/

        return search;
    };

    $.fn.autocomplete.defaults  = {
        maxHeight: 200,
        emptyMessage: 'No Matches Found...'
    };
}(window.jQuery));
