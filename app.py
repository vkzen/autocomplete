from elasticsearch import Elasticsearch
import json
from flask import Flask, render_template, request, json, jsonify
app = Flask(__name__)
 
@app.route("/")
def hello():
    return render_template('index.html')

@app.route('/complex')
def complex():
    es = Elasticsearch([{'host': 'localhost', 'port': 9200}])
    value = request.args.get('search')
    search = es.search(index="sw", body={"query": {"match": {'about': value}}})
    searchAbout = [s['_source']['about'] for s in search['hits']['hits']]
    return jsonify(searchAbout)
 
if __name__ == "__main__":
    app.run()
